<?php

namespace Gamma\Routing\Controller;


use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteria;

class RouterTest implements \Magento\Framework\App\RouterInterface
{
    protected $actionFactory;
    protected $_response;
    protected $pageRepository;
    protected $searchCriteria;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response,
        PageRepositoryInterface $pageRepository,
        SearchCriteria $searchCriteria
    )
    {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
        $this->pageRepository = $pageRepository;
        $this->searchCriteria = $searchCriteria;
    }
    public function match(\Magento\Framework\App\RequestInterface $request)
    {

        $identifier = trim($request->getPathInfo(), '/');
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$searchCriteriaBuilder = $objectManager->create('Magento\Framework\Api\SearchCriteriaBuilder');
        //$pages = $this->pageRepository->getList($searchCriteriaBuilder->create());
        //var_dump($pages);
        //die($identifier);
        if(strpos($identifier, 'routes-testing') === false) return false;
        return $this->actionFactory->create(
            'Gamma\Routing\Controller\Testing\index'
        );
    }
}
