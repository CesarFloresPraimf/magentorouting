<?php
namespace Gamma\Routing\Controller;

class RouterHome implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;
    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response
    )
    {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $static_pages = array('about-us', 'enable-cookies', 'no-route', 'customer-service', 'contact', 'privacy-policy-cookie-restriction-mode');
        if(in_array($identifier, $static_pages)) {
            $request->setModuleName('cms')
                ->setControllerName('page')
                ->setActionName('view');
        } else {
            return false;
        }

        return $this->actionFactory->create(
            'Gamma\Routing\Controller\Testing\RedirectHome'
        );
    }
}
