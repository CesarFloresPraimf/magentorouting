<?php

namespace Gamma\Routing\Controller\Testing;


use Magento\Framework\App\Action\Action;

class RedirectHome extends Action {

    public function execute(){
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('home');
        return $resultRedirect;
    }
}


